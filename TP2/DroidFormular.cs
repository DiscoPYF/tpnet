﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1;

namespace TP2
{
    public struct DroidFormular
    {
        private bool isChecked;
        public bool IsChecked
        {
            get
            {
                return (this.isChecked);
            }
            set
            {
                if (this.isChecked != value)
                    this.isChecked = value;
            }
        }

        private string visualName;
        public string VisualName
        {
            get
            {
                return (this.visualName);
            }
            set
            {
                if (this.visualName != value)
                    this.visualName = value;
            }
        }


        private Type type;
        public Type Type
        {
            get
            {
                return (this.type);
            }
            set
            {
                if (this.type != value)
                    this.type = value;
            }
        }


        public DroidFormular(bool isChecked, string visualName, Type type)
        {
            this.isChecked = isChecked;
            this.visualName = visualName;
            if (type != typeof(Droid))
                throw new NotSupportedException("Invalid type, it must be a droid");
            else
                this.type = type;
        }
    }
}
