﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using TP1;
using System.Linq;
using System.Windows.Controls;

namespace TP2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Property

        private DroidFactory droidFactory;
        public DroidFactory DroidFactory
        {
            get
            {
                return (this.droidFactory);
            }
            set
            {
                if (this.droidFactory != value)
                {
                    this.droidFactory = value;
                    this.NotifyPropertyChanged("DroidFactory");
                }
            }
        }

        private ObservableCollection<Droid> droids;
        public ObservableCollection<Droid> Droids
        {
            get
            {
                return (this.droids);
            }
            set
            {
                if (this.droids != value)
                {
                    this.droids = value;
                    this.NotifyPropertyChanged("Droids");
                }
            }
        }

        private IList<Type> droidTypes;
        public IList<Type> DroidTypes
        {
            get
            {
                return (this.droidTypes);
            }
            set
            {
                if (this.droidTypes != value)
                {
                    this.droidTypes = value;
                    this.NotifyPropertyChanged("DroidTypes");
                }
            }
        }

        public IList<Type> DroidTypesForFactory { get; private set; }

        /// <summary>
        /// Set in constructor
        /// </summary>
        private Type defaultType;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            this.defaultType = typeof(R2D2);
        }

        #endregion

        #region EventHandler

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DroidFactory = new DroidFactory();
            this.DroidTypes = new List<Type>()
                {
                    typeof(R2D2),
                    typeof(C3PO),
                    typeof(BattleDroid),
                    typeof(Astromech),
                };
            this.Droids = new ObservableCollection<Droid>();
            this.DroidTypesForFactory = new List<Type>();
            this.DroidFactory.DroidCreated += new EventHandler<Droid>((o, d) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.Droids.Add(d);
                });
            });
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            this.DroidFactory.Start(this.DroidTypesForFactory);
            this.Display.Text = "Factory is running....";
            this.Droids.Clear();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            this.DroidFactory.Stop();
            this.Display.Text = "Factory is not running";
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Type type;

            type = (sender as CheckBox).DataContext as Type;
            this.DroidTypesForFactory.Add(type);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Type type;

            type = this.DroidTypesForFactory.Where((x) => x.Name == ((sender as CheckBox).DataContext as Type).Name).First();
            this.DroidTypesForFactory.Remove(type);
        }

        private void CheckBox_Loaded(object sender, RoutedEventArgs e)
        {
            if ((sender as CheckBox).DataContext as Type == defaultType)
                (sender as CheckBox).IsChecked = true;
        }

        #endregion

        #region PropertyNotification

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
