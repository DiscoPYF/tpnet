﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class DroidCreatedArg
    {
        public Droid Droid { get; private set; }

        public DroidCreatedArg(Droid droid)
        {
            this.Droid = droid;
        }
    }
}
