﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    public class BattleDroid : Droid
    {
        public override void Init()
        {
            base.Init();

            Console.Write("Hi. I'm BattleDroid, a very famous droid.\n\n");
            this.Name = "       _____\n     .'/L|__`.\n    / =[_]O|` \\\n    |\"+_____\":|\n  __:='|____`-:__\n ||[] ||====| []||\n ||[] | |=| | []||\n |:||_|=|U| |_||:|\n |:|||]_=_ =[_||:|\n | |||] [_][]C|| |\n | ||-'\"\"\"\"\"`-|| |\n  /|\\_\\_|_|_/_/|\\\n";
        }

        public override void ShutDown()
        {
            Console.Write("BattleDroid has shutdown.\n");
        }
    }
}
