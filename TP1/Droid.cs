﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TP1
{
    public abstract class Droid : INotifyPropertyChanged
    {
        #region Property

        private string name;
        public string Name
        {
            get
            {
                return (this.name);
            }
            protected set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.NotifyPropertyChanged("Name");
                }
            }
        }

        private string type;
        public string Type
        {
            get
            {
                return (this.type);
            }
            set
            {
                if (this.type != value)
                {
                    this.type = value;
                    this.NotifyPropertyChanged("Type");
                }
            }
        }

        private bool isInitialized;

        #endregion

        #region Constructor

        public Droid()
        {
            this.Type = this.GetType().Name;
        }

        #endregion

        #region Method

        public virtual void Init()
        {
            Console.Write("Initializing droid...\n");
            Thread.Sleep(new TimeSpan(0, 0, 1));
            this.Name = "Droid";

            this.isInitialized = true;
            Console.Write("\nDroid initialized.\n");
        }

        public bool Work()
        {
            if (this.isInitialized)
                return (true);
            else
                return (false);
        }

        public virtual void ShutDown()
        {
            if (!this.isInitialized)
                throw new NotSupportedException("Droid isn't initialized");
            else
                Console.Write("\nDroid has shutdown.\n");
        }

        #endregion

        #region PropertyNotification

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
