﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TP1
{
    public class DroidFactory : INotifyPropertyChanged
    {
        private IList<Type> types;
        public EventHandler<Droid> DroidCreated { get; set; }
        System.Timers.Timer timer;
        private int delay;
        public int Delay
        {
            get
            {
                return (this.delay);
            }
            set
            {
                if (this.delay != value)
                {
                    this.delay = value;
                    this.timer.Interval = TimeSpan.FromSeconds(this.delay).TotalMilliseconds;
                    this.NotifyPropertyChanged("Delay");
                }
            }
        }

        private bool isRunning;
        public bool IsRunning
        {
            get
            {
                return (this.isRunning);
            }
            set
            {
                if (this.isRunning != value)
                {
                    this.isRunning = value;
                    this.NotifyPropertyChanged("IsRunning");
                }
            }
        }

        public DroidFactory(int delay = 1)
        {
            timer = new System.Timers.Timer(TimeSpan.FromSeconds(delay).TotalMilliseconds);
            this.delay = delay;
            this.IsRunning = false;
        }

        public T BuildDroid<T>() where T : Droid, new()
        {
            T result;

            result = new T();

            return (result);
        }

        public void Start(IList<Type> types)
        {
            Droid droid;
            Random random;

            timer.Interval = TimeSpan.FromSeconds(this.Delay).TotalMilliseconds;
            this.types = types;
            random = new Random();
            timer.Elapsed += new System.Timers.ElapsedEventHandler((o, e) =>
            {
                droid = (Droid)Activator.CreateInstance(types[random.Next(this.types.Count)]);
                droid.Init();
                droid.Work();
                droid.ShutDown();
                if (this.DroidCreated != null)
                    this.DroidCreated(this, droid);
            });
            timer.AutoReset = true;
            timer.Start();
            this.IsRunning = true;
            Console.Write("** Factory has started **\n\n");
        }

        public void Stop()
        {
            this.timer.Stop();
            this.IsRunning = false;
        }

        #region PropertyNotification

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
